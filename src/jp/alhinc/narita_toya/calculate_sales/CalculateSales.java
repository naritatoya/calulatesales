package jp.alhinc.narita_toya.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		System.out.println("ここにあるファイルを開きます =>" + args[0]);

		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst.txt");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			Map<Object,Object> map = new HashMap<>();

			String line;

			while ((line = br.readLine()) != null) {
					System.out.println(line);

					String result = line.substring(0,4);
					String result2 = line.substring(4);

					map.put(result,result2);
			}

		} catch (IOException e) {
			System.out.println("エラーが発生しました。");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
	}
}